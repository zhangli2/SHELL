#!/bin/bash
#install lamp
#author zl 2019

#display the menu
echo -e "\033[32mPlease select your menu:\033[0m"
echo "1) Install Apache server"
echo "2) Install Mysql server"
echo "3) Install Php Server"
echo -e "\033[31mUsage:(menu.sh 1|2|3)\033[0m"

read menu
echo $menu

#mount the cdrom
if [ ! -d /media/cdrom ]
then
	mkdir -p /media/cdrom && echo "create the /media/cdrom"
else 
	echo "the /media/cdrom is exsit"
fi

umount /dev/sr0 &> /dev/null
mount /dev/sr0 /media/cdrom

#select 1
if [ $menu -eq 1 ]
then
	cd /media/cdrom/Packages
        rpm -ivh apr-1.4.8-3.el7_4.1.x86_64.rpm
	rpm -ivh apr-devel-1.4.8-3.el7_4.1.x86_64.rpm
	rpm -ivh cyrus-sasl-devel-2.1.26-23.el7.x86_64.rpm
	rpm -ivh expat-devel-2.1.0-10.el7_3.x86_64.rpm
	rpm -ivh libdb-devel-5.3.21-24.el7.x86_64.rpm
	rpm -ivh openldap-devel-2.4.44-13.el7.x86_64.rpm
	rpm -ivh apr-util-devel-1.5.2-6.el7.x86_64.rpm
	rpm -ivh apr-util-1.5.2-6.el7.x86_64.rpm
	rpm -ivh pcre-devel-8.32-17.el7.x86_64.rpm
	rpm -ivh pcre-8.32-17.el7.x86_64.rpm
        cd /usr/src
	tar zxvf httpd-2.4.38.tar.gz -C /usr/src
	cd /usr/src/httpd-2.4.38
	./configure  --prefix=/usr/local/httpd --enable-so --enable-rewrite --enable-charset-lite --enable-cgi
	make && make install
	if [  $? -eq 0 ]
	then
		echo "Apache sever install success"
	else
		exit
	fi
	/usr/local/httpd/bin/apachectl start &> /dev/null
fi

#select 2
if [ $menu -eq 2 ]
then
        cd /media/cdrom/Packages
        rpm -ivh ncurses-devel-5.9-14.20130511.el7_4.x86_64.rpm
	cd /usr/src
        tar zxvf cmake-2.8.6.tar.gz
        cd /usr/src/cmake-2.8.6
        ./configure
	gmake && gmake install
	cd /usr/src
	tar zxvf mysql-5.6.36.tar.gz
	cd /usr/src/mysql-5.6.36 
	cmake -DCMAKE_INSTALL_PREFIX=/usr/local/mysql -DSYSCONFDIR=/etc -DDEFAULT_CHARSET=utf8 -DDEFAULT_COLLATION=utf8_general_ci -DWITH_EXTRA_CHARSETS=all        
	make && make install
        if [  $? -eq 0 ]
        then
                echo "Mysql sever install success"
        
	else
		exit
	fi
	rm -rf /etc/my.cnf
	cp /usr/local/mysql/support-files/my-default.cnf /etc/my.cnf
	cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysqld 
	chmod +x /etc/rc.d/init.d/mysqld 
	chkconfig --add mysqld
	echo "PATH=$PATH:/usr/local/mysql/bin" >> /etc/profile
	export PATH=$PATH:/usr/local/mysql/bin
	groupadd mysql
	useradd -M -s /sbin/nologin mysql -g mysql
	chown -R mysql:mysql /usr/local/mysql/
	/usr/local/mysql/scripts/mysql_install_db --basedir=/usr/local/mysql --datadir=/usr/local/mysql/data --user=mysql  &> /dev/null
	systemctl start mysqld
	systemctl enable mysqld
	mysqladmin -u root password 'pwd123'
fi

#select 3
if [ $menu -eq 3 ]
then
        cd /media/cdrom/Packages
        rpm -ivh gc-7.2d-7.el7.x86_64.rpm 
	rpm -ivh zlib-devel-1.2.7-17.el7.x86_64.rpm 
	rpm -ivh xz-devel-5.2.2-1.el7.x86_64.rpm
	rpm -ivh libxml2-devel-2.9.1-6.el7_2.3.x86_64.rpm 
	cd /usr/src
        tar zxvf php-5.5.38.tar.gz
        cd /usr/src/php-5.5.38
        ./configure --prefix=/usr/local/php5 --with-mysqli=/usr/local/mysql/bin/mysql_config  --with-apxs2=/usr/local/httpd/bin/apxs --with-mysql=/usr/local/mysql --with-config-file-path=/usr/local/php5 --enable-mbstring 
	make && make install
        if [  $? -eq 0 ]
        then
                echo "Php sever install success"
        else 
		exit
	fi
	cp /usr/src/php-5.5.38/php.ini-development /usr/local/php5/php.ini
	cd /usr/src
	tar zxvf zend-loader-php5.5-linux-x86_64_update1.tar.gz zend-loader-php5.5-linux-x86_64
	cp /usr/src/zend-loader-php5.5-linux-x86_64/ZendGuardLoader.so /usr/local/php5/lib/php/
	echo "zend_extension=/usr/local/php5/lib/php/ZendGuardLoader.so" >>/usr/local/php5/php.ini 
	echo "zend_loader.enable=1" >>/usr/local/php5/php.ini
	if [  $? -eq 0 ]
        then
                echo "Zend_loader install success"
        else
                exit
        fi
	

fi

