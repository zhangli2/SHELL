#!/bin/bash
#定义网段地址、MAC地址文件
FILE=/root/ethers
NADD=192.168.137.
#判断文件/root/ethers是否存在
[ -f $FILE  ] && /usr/bin/cp $FILE $FILE.BAK
#PING测试是否存在主机
HADD=1
while [ $HADD -lt 254  ]
do 
  ping -c 3 "$NADD$HADD" &> /dev/null
  if [ $? -eq 0 ]
  then
    arp -n | grep ether| grep "$NADD$HADD" | awk '{print $1,$3}' >> $FILE  
    echo "$NADD$HADD is up."
  else
    echo "$NADD$HADD is down."
  fi
  let HADD++
done 
