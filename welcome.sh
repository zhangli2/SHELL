#!/bin/bash
#此脚本用于显示进程数，登录的用户数和用户名，根分区的磁盘使用率
echo "已开启的进程数：$(expr $(ps aux | wc -l) - 2)"
echo "已登录的用户数：$(who | wc -l)"
echo "已登录的用户账号：$(who | awk '{print $1}')"
echo "根分区磁盘使用率：$(df -h| grep "/$"| awk '{print $5}')"



