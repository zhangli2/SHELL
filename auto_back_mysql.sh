#!/bin/bash
#auto backup mysql db
#before you back ,please grant to the user backup
#create database discuz
#grant all on discuz.* to backup@'localhost' identified by "123456"
#flush privileges
#author zl 2019

#define backup path
BAK_DIR=/data/backup/`date +%Y%m%d`
MYSQLDB=discuz
MYSQLUSR=backup
MYSQLPWD=123456
MYSQLCMD=/usr/bin/mysqldump

#define must to use root
if [ $UID -ne 0 ];then
	echo "You must to be use root"
	exit
fi

#judge the dir is exist,if not create it
if [ ! -d $BAK_DIR ];then
	mkdir -p $BAK_DIR && echo -e "\033[32mcreate the backdir success\033[0m"
else
	echo "THIS $BAK_DIR IS EXSITS..."
fi

#run the backup
mysqldump -u$MYSQLUSR -p$MYSQLPWD $MYSQLDB > $BAK_DIR/$MYSQLDB.sql

#judge the backup is ok
if [ $? -eq 0 ];then
	echo -e "\033[32mTHE mysql backup $MYSQLDB successful!\033[0m"
else
	echo -e "\033[32mPlese check you script!\033[0m"
fi
