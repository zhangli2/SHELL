#!/bin/bash
#install lamp
#author zl 2019

#display the menu
echo -e "\033[32mPlease select your menu:\033[0m"
echo "1) Install Apache server"
echo "2) Install Mysql server"
echo "3) Install Php Server"
echo -e "\033[31mUsage:(menu.sh 1|2|3)\033[0m"

read menu
echo $menu

#mount the cdrom
if [ ! -d /media/cdrom ]
then
	mkdir -p /media/cdrom && echo "create the /media/cdrom"
else 
	echo "the /media/cdrom is exsit"
fi

umount /dev/sr0 &> /dev/null
mount /dev/sr0 /media/cdrom

#select 1
if [ $menu -eq 1 ]
then
	cd /media/cdrom/Packages
        rpm -ivh apr-1.4.8-3.el7_4.1.x86_64.rpm
	rpm -ivh apr-devel-1.4.8-3.el7_4.1.x86_64.rpm
	rpm -ivh cyrus-sasl-devel-2.1.26-23.el7.x86_64.rpm
	rpm -ivh expat-devel-2.1.0-10.el7_3.x86_64.rpm
	rpm -ivh libdb-devel-5.3.21-24.el7.x86_64.rpm
	rpm -ivh openldap-devel-2.4.44-13.el7.x86_64.rpm
	rpm -ivh apr-util-devel-1.5.2-6.el7.x86_64.rpm
	rpm -ivh apr-util-1.5.2-6.el7.x86_64.rpm
	rpm -ivh pcre-devel-8.32-17.el7.x86_64.rpm
	rpm -ivh pcre-8.32-17.el7.x86_64.rpm
        cd /usr/src
	tar zxvf httpd-2.4.38.tar.gz -C /usr/src
	cd /usr/src/httpd-2.4.38
	./configure  --prefix=/usr/local/httpd --enable-so --enable-rewrite --enable-charset-lite --enable-cgi
	make && make install
	if [  $? -eq 0 ]
	then
		echo "Apache sever install success"
	fi
	/usr/local/httpd/bin/apachectl start
fi
